


#include <Arduino.h>
#include "bbot.h"


struct {
  int Left_Motor_Forward = 7;
  int Right_Motor_Forward = 8;
  int Left_Motor_Backward = 6;
  int Right_Motor_Backward = 9;
  int Right_UltraSonic_Trig = 47;
  int Left_UltraSonic_Trig = 49;
  int Back_UltraSonic_Trig = 44;
  int Right_UltraSonic_echo = 46;
  int Left_UltraSonic_echo = 48;
  int Back_UltraSonic_echo = 45;
  int Right_Light = A0;
  int Left_Light = A2;
  int Middle_Light = A1;
} constant;


namespace bbot
{
    // public:
      namespace input
      {
          namespace sensors
          {
            int Read_Light(Location location)
            {
              int value = 0;
              if (location == Middle)
              {
                value = analogRead(A1);
              }
              else if (location == Right)
              {
                value = analogRead(A0);
              }
              else if (location == Left)
              {
                value = analogRead(A2);
              }
              value = (value) / 6.5;
              value = max(0, value);
              value = min(100, value);
              return value;
            }

            int Read_Ultrasonic(Location location)
            {
              int echo, trig;
              if (location == Back)
              {
                trig = constant.Back_UltraSonic_Trig;
                echo = constant.Back_UltraSonic_echo;
              }
              else if (location == Right)
              {
                trig = constant.Right_UltraSonic_Trig;
                echo = constant.Right_UltraSonic_echo;
              }
              else if (location == Left)
              {
                trig = constant.Left_UltraSonic_Trig;
                echo = constant.Left_UltraSonic_echo;
              }

              digitalWrite(trig, LOW);
              delayMicroseconds(2);
              digitalWrite(trig, HIGH);
              delayMicroseconds(7);
              digitalWrite(trig, LOW);
              float time = pulseIn(echo, HIGH);
              return 10 * time / 58;
            }
          }
      }

      namespace output
      {
        namespace motors
        {
          void move_motor(Location motor_location, int speed)
          {
            speed *= 1.5;
            speed += 100;
            if (speed == 100)
              speed = 0;

            if (speed >= 0)
            {
              if ( motor_location == Right)
              {
                analogWrite(constant.Right_Motor_Backward, 0);
                analogWrite(constant.Right_Motor_Forward, speed);
              }
              else
              {
                analogWrite(constant.Left_Motor_Backward, 0);
                analogWrite(constant.Left_Motor_Forward, speed);
              }
            }
            else
            {
              if ( motor_location == Right)
              {
                analogWrite(constant.Right_Motor_Forward, 0);
                analogWrite(constant.Right_Motor_Backward, -speed);
              }
              else
              {
                analogWrite(constant.Left_Motor_Forward, 0);
                analogWrite(constant.Left_Motor_Backward, -speed);
              }
            }

          }


          void move_steering(float speed, float steer)
          {
            speed *= 1.5;
            speed += 100;
            if (speed == 100)
              speed = 0;
            steer += 1.5;


            if (speed >= 0)
            {
              if (abs(steer) > 50)
              {
                if (steer < 0)
                {
                  analogWrite(constant.Right_Motor_Backward, 0);
                  analogWrite(constant.Left_Motor_Forward, 0);
                  analogWrite(constant.Right_Motor_Forward, min(254, abs(speed) * (2 - (50 + steer) * 0.01)) );
                  analogWrite(constant.Left_Motor_Backward, abs(speed * (50 + steer) * 0.01));
                }
                else
                {
                  analogWrite(constant.Right_Motor_Forward, 0);
                  analogWrite(constant.Left_Motor_Backward, 0);


                  analogWrite(constant.Right_Motor_Backward, abs( speed * (steer - 50) * 0.01));
                  analogWrite(constant.Left_Motor_Forward, min (254, abs(speed * (2 - (steer - 50) * 0.01))));
                }
              }
              else
              {
                analogWrite(constant.Right_Motor_Backward, 0);
                analogWrite(constant.Left_Motor_Backward, 0);
                if (steer < 0)
                {

                  analogWrite(constant.Right_Motor_Forward, min(abs(speed) * (2 - (1 + steer * 0.01)), 254));
                  analogWrite(constant.Left_Motor_Forward, abs(speed * (1 + steer * 0.01)));
                }
                else
                {
                  analogWrite(constant.Right_Motor_Forward, abs(speed * (1 - steer * 0.01)));
                  //Serial.println(abs(speed*(1-steer*0.01)));
                  analogWrite(constant.Left_Motor_Forward, min (254, abs(speed) * (2 - (1 - steer * 0.01))));
                  //Serial.println(abs(speed)*(2-(1-steer*0.01)));
                }
              }
            }
            else
            {
              if (abs(steer) > 50)
              {
                if (steer < 0)
                {
                  analogWrite(constant.Right_Motor_Forward, 0);
                  analogWrite(constant.Left_Motor_Backward, 0);
                  analogWrite(constant.Right_Motor_Backward, min(254, abs(speed) * (2 - (50 + steer) * 0.01)));
                  analogWrite(constant.Left_Motor_Forward, abs(speed * (50 + steer) * 0.01));
                }
                else
                {
                  analogWrite(constant.Right_Motor_Backward, 0);
                  analogWrite(constant.Left_Motor_Forward, 0);
                  analogWrite(constant.Right_Motor_Forward, abs( speed * (steer - 50) * 0.01));
                  analogWrite(constant.Left_Motor_Backward, min(254, abs(speed * (2 - (steer - 50) * 0.01))));
                }
              }
              else
              {
                analogWrite(constant.Right_Motor_Forward, 0);
                analogWrite(constant.Left_Motor_Forward, 0);
                if (steer < 0)
                {
                  analogWrite(constant.Right_Motor_Backward, min(254, abs(speed) * (2 - (1 + steer * 0.01))));
                  analogWrite(constant.Left_Motor_Backward, abs(speed * (1 + steer * 0.01)));
                }
                else
                {
                  analogWrite(constant.Right_Motor_Backward, abs(speed * (1 - steer * 0.01)));
                  analogWrite(constant.Left_Motor_Backward, min(254, abs(speed) * (2 - (1 - steer * 0.01))));
                }
              }
            }
          }

        }
      }
}
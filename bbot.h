
enum Location
{
  Right, Left, Back, Middle
};

namespace bbot
{
    // public:
      namespace input
      {
          namespace sensors
          {
            int Read_Light(Location location);
            

            int Read_Ultrasonic(Location location);
       	}
            
      }

      namespace output
      {
        namespace motors
        {
          void move_motor(Location motor_location, int speed);

          void move_steering(float speed, float steer);
          

        }
      }
}


/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://www.arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  by Scott Fitzgerald
 */
#include "bbot.h"


// the setup function runs once when you press reset or power the board
void setup()
{
  // initialize digital pin 13 as an output.
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);
  Serial.begin(115200);
  Serial.flush();
  Serial.setTimeout(0);

}


// the loop function runs over and over again forever
void loop()
{
//  Robot bbot;
  float x, oldx = 0, sum = 0, res;

  /*(//bbot.Read_Ultrasonic(Right);
   while (1)
   {
    x=bbot.Read_Light(Middle)-35 ;
    if (x>0 && sum<0)
      sum=0;
    else if (x<0 && sum >0)
      sum=0;
   sum+=x;
   res=x*1 +(x-oldx)*0.4+sum*0.004;

  // Serial.println(res);
  bbot.move_steering(60,res);
   delay(1);
  // delay(100);
  //Serial.println(bbot.Read_Light(Middle));

   oldx=x;
   }*/
  bbot::output::motors::move_motor(Left, 20);



  // wait for a second
}


